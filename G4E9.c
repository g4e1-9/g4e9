//GUIA MANEJO STRINGS EJERCICIO 9
//El usuario ingresará una palabra de hasta 10 letras.
//Se desea mostrarla en pantalla pero encriptada según el "Código César".
//Esto consiste en reemplazar cada letra con la tercera consecutiva en el abecedario.
//Por ejemplo, "CASA" se convierte en "FDVD".
//Tener en cuenta que las últimas letras deben volver al inicio, por ejemplo la Y se convierte B.
//Este mecanismo se utilizaba en el Imperio Romano.

#include <stdio.h>
#include <string.h>

int main()
{
    char texto[9]; //Declaro las variables.
    char letras[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v','w', 'x','y','z'};
    int i, j;
    
printf("Ingrese el texto a encriptar: ");
fgets(texto, 9, stdin);

for (i = 0; i < strlen(texto); i++) //Recorro la cantidad de caracteres que hay en la palabra y si es igual a las letras se suman 3 a su valor.
{
    for(j = 0; j <= 26; j++)
    {
        if(texto[i] == letras[j])
        {
            int cod = (j + 3) % 26;
            texto[i] = letras[cod];
            break;
        }
    }
}
    
printf("El texto codificado es: %s", texto); //Muestro en pantalla el texto codificado.

return 0;

}
